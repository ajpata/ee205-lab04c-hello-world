///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04c - Hello World
//
// Usage:  hello2
//
// Result:
//   Prints hello world without using namespaces
//
// @author AJ Patalinghog <ajpata@hawaii.edu>
// @date   14 February 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

int main() {
   
   std::cout << "Hello World!" << std::endl;

   return 0;

}
