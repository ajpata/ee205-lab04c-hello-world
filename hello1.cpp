///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04c - Hello World
//
// Usage:  hello1
//
// Result:
//   Prints hello world using namespaces
//
// @author AJ Patalinghog <ajpata@hawaii.edu>
// @date   14 February 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
using namespace std;

int main() {
   
   cout << "Hello World!" << endl;

   return 0;

}
