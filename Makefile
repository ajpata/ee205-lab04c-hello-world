###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 04c - Hello World
#
# @file    Makefile
# @version 1.0
#
# @author AJ Patalinghog <ajpata@hawaii.edu>
# @brief  Lab 04c - Hello World - EE 205 - Spr 2021
# @date   14 February 2021
###############################################################################

TARGETS = hello1 hello2

all: $(TARGETS)

hello1: hello1.cpp
	g++ -g -Wall -o hello1 hello1.cpp

hello2: hello2.cpp
	g++ -g -Wall -o hello2 hello2.cpp

clean:
	rm -f *.o $(TARGETS)

